#!/usr/bin/env python3

from copy import deepcopy
from collections import deque
from class_graph import *

Branco = 0
Cinza = 1
Preto = 2

def BFS(g: Grafo, v: int = 0):
    """Busca em largura"""

    # Cria a lista de vertices como valores padrões
    # vertice = {"cor", "distancia", "antecessor"}
    vertices = []
    for i in range(0, g.nVertices):
        vertices.append({"cor":Branco, "distancia":float('inf'), "antecessor":None})

    # Inicializa o primeiro vertice
    vertices[v]["cor"] = Cinza
    vertices[v]["distancia"] = 0

    # Cria a fila e enfileira o 'v'
    fila = deque()
    fila.append(v)

    while(len(fila)):
        u = fila.popleft()
        for i in g.vizinhos(u):
            if(vertices[i]["cor"] == Branco):
                vertices[i]["cor"] = Cinza
                vertices[i]["distancia"] = vertices[u]["distancia"] + 1
                vertices[i]["antecessor"] = u
                fila.append(i)

        vertices[u]["cor"] = Preto

    return vertices


def DFS(g: Grafo, v: int = 0, ordem:list = []):
    """Busca em Profundidade"""

    default = list(range(0, g.nVertices))

    for i in ordem:
        default.remove(i)

    ordem += default

    # Criar a lista de vertices
    # vetice = {"cor", "antecessor", "chegada", "saida"}
    vertices = []
    for i in range(0, g.nVertices):
        vertices.append({"vertice":i, "cor":Branco, "antecessor":None, "chegada":-1, "saida":-1})

    tempo = 0
    for u in ordem:
        if(vertices[u]["cor"] == Branco):
            tempo = DFSVisit(g, u, tempo, vertices)

    return vertices


def DFSVisit(g:Grafo, u:int, tempo:int, vertices:list):
    # Incrementer o tempo e atribuir a cheagada do do vertice
    tempo += 1
    vertices[u]["chegada"] = tempo
    vertices[u]["cor"] = Cinza

    for v in g.vizinhos(u):
        if(vertices[v]["cor"] == Branco):
            vertices[v]["antecessor"] = u
            tempo = DFSVisit(g, v, tempo, vertices)

    vertices[u]["cor"] = Preto
    tempo += 1
    vertices[u]["saida"] = tempo

    return tempo

# =============================================================================

def BuscarPontes(g:Grafo):
    """Achar todas as pontes do grafo, retornar uma tupla (nArestas, (arestas))

    Na árvore da DFS uma aresta a=(u, v), onde u é o antecessor de v na árvore DFS,
    é uma ponte se não existe outra alternativa para alcançar u ou os antecessores
    de u apartir da sub-árvore com raiz em v. Para isso, garda-se o em 'low' o
    tempo de descoberta do primeiro vértice alcansável apartir da sub-arvore com
    raiz em v. a=(u, v) é ponte se u.low > v.achado."""

    pontes = []

    # Criar lista de vertices
    # vertice = {cor, antecessor, chegada, low}
    vertices = []
    for i in range(0, g.nVertices):
        vertices.append({"visitado":False, "antecessor":None, "chegada":-1, "low":-1})

    tempo = 0
    # Fazer a busca em cada componete conexa
    for u in range(0, g.nVertices):
        if(not vertices[u]["visitado"]):
            BuscarPontesAux(g, u, tempo, vertices, pontes)

    return (len(pontes), tuple(pontes))


def BuscarPontesAux(g:Grafo, u:int, tempo:int, vertices:list, pontes:list):
    # Marcar como visitado
    vertices[u]["visitado"] = True

    # Inicializar o tempo de chegada e low
    tempo += 1
    vertices[u]["chegada"] = vertices[u]["low"] = tempo

    # Visitar todos os vizinhos
    for v in g.vizinhos(u):
        # Visitar o vértice
        if(not vertices[v]["visitado"]):
            vertices[v]["antecessor"] = u
            tempo = BuscarPontesAux(g, v, tempo, vertices, pontes)

            # Verificar se a sub árvore com raiz em v tem coneção com um dos
            # antecessores de u
            vertices[u]["low"] = min(vertices[u]["low"], vertices[v]["low"])

            # Se o menor tempo de chagada na sub-arvore com vertice em v é maior
            # que o tempo de descoberta de u, então (u, v) é uma ponte
            if(vertices[v]["low"] > vertices[u]["chegada"]):
                pontes.append((u, v))

        # Atualizar 'low'
        elif(v != vertices[u]["antecessor"]):
            vertices[u]["low"] = min(vertices[u]["low"], vertices[v]["chegada"])

    # A função retorna o tempo atual para continuar com a contagem
    return tempo

# =============================================================================

def BuscarVerticesArticulacao(g:Grafo):
    """Achar todas os vertices de articulação do grafo, retorna uma tupla (nVertices, (vertices))"""
    va = []

    # Criar lista de vertices
    # vertice = {visitado, antecessor, chegada, low}
    vertices = []
    for i in range(0, g.nVertices):
        vertices.append({"visitado":False, "antecessor":None, "chegada":-1, "low":-1})

    tempo = 0
    # Fazer a busca em cada componete conexa
    for u in range(0, g.nVertices):
        if(not vertices[u]["visitado"]):
            BuscarVerticesArticulacaoAux(g, u, tempo, vertices, va)

    va = list(set(va))
    return (len(va), tuple(va))


def BuscarVerticesArticulacaoAux(g:Grafo, u:int, tempo:int, vertices:list, va:list):
    # Marcar como visitado
    vertices[u]["visitado"] = True

    # Contador de vertices filhos
    filhos = 0

    # Inicializar o tempo de chegada e low
    tempo += 1
    vertices[u]["chegada"] = vertices[u]["low"] = tempo

    # Visitar todos os vizinhos
    for v in g.vizinhos(u):
        # Visitar o vértice
        if(not vertices[v]["visitado"]):
            filhos += 1
            vertices[v]["antecessor"] = u
            tempo = BuscarVerticesArticulacaoAux(g, v, tempo, vertices, va)

            # Verificar se a sub árvore com raiz em v tem coneção com um dos
            # antecessores de u
            vertices[u]["low"] = min(vertices[u]["low"], vertices[v]["low"])

            # U será vertice de articulação se
            # 1. u é raiza da árvore DFS e tem mais de um filho
            if((vertices[u]["antecessor"] == None) and (filhos > 1)):
                va.append(u)

            # 2. se não é raiz e o menor tempo de um de seus filhos é maior
            # que o de u
            if((vertices[u]["antecessor"] != None) and (vertices[v]["low"] >= vertices[u]["chegada"])):
                va.append(u)


        # Atualizar 'low'
        elif(v != vertices[u]["antecessor"]):
            vertices[u]["low"] = min(vertices[u]["low"], vertices[v]["chegada"])

    # A função retorna o tempo atual para continuar com a contagem
    return tempo

# =============================================================================

def AlcansarNVertices(g:Grafo, v:int):
    b = BFS(g, v)
    n = 0
    for i in b:
        if(i["distancia"] != float('inf')):
            n += 1

    return n

# =============================================================================

def Warshall(g:Grafo):
    if(g.tipoEstrutura != 0):
        w = deepcopy(g)
        w.gerarMA()
        w = deepcopy(w.estrutura)
    else:
        w = deepcopy(g.estrutura)


    for k in range(0, g.nVertices):
        # print("k: {}".format(k))
        for i in range(0, g.nVertices):
            # print("  i: {}".format(i))
            for j in range(0, g.nVertices):
                # print("    j: {}".format(j))
                w[i][j] = w[i][j] or (w[i][k] and w[k][j])

        return(w)

# =============================================================================

def GrafoSConexo(g:Grafo):
    # Fazer a DFS
    b = DFS(g, 0)
    for i in range(0, g.nVertices):
        b[i]["vertice"] = i

    # Achar as arvores da arvore DFS
    arvores = []
    for i in b:
        if(i["antecessor"] == None):
            arvores.append([i])

    # Remover as arvores da arvore DFS
    for i in arvores:
        b.remove(i[0])

    # Achar os vértices que fazem parte das arvores da DFS
    for v in arvores:
        for u in b:
            # Se o tempo de saida do vertice for maior do a da raiz e
            # o tempo chegada é maior do o da raiz, o vertice faz parte da sub-arvore
            # da raiz
            if((u["saida"] < v[0]["saida"]) and (u["chegada"] > v[0]["chegada"])):
                v.append(u)

    # Se so tem um arvore BFS, o grafo é conexo
    if(len(arvores) == 1):
        return True

    # Criar uma matriz vazia
    h = []
    for i in range(0, len(arvores)):
        h.append([])
        for j in range(0, len(arvores)):
            h[i].append(0)

    # Achar ligação entre as arestas
    for i in range(0, len(arvores)):
        for j in range(0, len(arvores)):
            # Pula a propria list
            if(i == j):
                continue

            # Para cara vertice na arvore i
            for u in arvores[i]:
                # Para cara vertice na arvore j
                for v in arvores[j]:
                    # se existe (u, v) ou (v, u) no Grafo
                    if(v in g.vizinhos(u["vertice"])):
                        h[i][j] = 1

    # Verificar se tem uma linha nula
    _n = []
    for i in range(0, len(arvores)):
        _n.append(0)

    if(_n in h):
        return False
    else:
        return True


def GrafoFConexo(w:list):
    """w precisa ser uma MA"""

    for i in range(0, len(w)):
        for j in range(0, len(w)):
            if(not (w[i][j] and w[j][i])):
                return  False

    return True


def GrafoSFConexo(w:list):
    """w precisa ser uma MA"""

    for i in range(0, len(w)):
        for j in range(0, len(w)):
            if(not (w[i][j] or w[j][i])):
                return  False

    return True

# ===================================================================

def ComponenteFConexa(g:Grafo):
    # Realizar a DFS
    d = DFS(g)

    # Copiar o Grafo
    gt = deepcopy(g)

    # Inverter as arestas
    for i in range(0, gt.nArestas):
        a = gt.arestas[i]
        gt.arestas[i] = (a[1], a[0], a[2])

    # Regerar a estrutura
    if(g.tipoEstrutura == 0):
        gt.gerarMA()
    elif(g.tipoEstrutura == 1):
        gt.gerarLA()
    elif(g.tipoEstrutura == 2):
        gt.gerarMI()

    # Ordenar os vertices pelo tempo de saida
    d = sorted(d, key=lambda k: k["saida"], reverse=True)
    ordem = []
    for i in d:
        ordem.append(i["vertice"])

    # Fazer a busca na transposta
    d2 = DFS(gt, ordem=ordem)

    # Achar as raizes das árvores
    arvores = [item for item in d2 if item["antecessor"] == None]
    for i in range(0, len(arvores)):
        arvores[i] = [arvores[i]]
    # print(arvores)

    for v in arvores:
        for u in d2:
            # Se o tempo de saida do vertice for maior do a da raiz e
            # o tempo chegada é maior do o da raiz, o vertice faz parte da sub-arvore
            # da raiz
            if((u["saida"] < v[0]["saida"]) and (u["chegada"] > v[0]["chegada"])):
                v.append(u)

    # Criando novos vértices
    novosVertices = []
    for i in range(0, len(arvores)):
        antigosVertices = []
        nv = "<"
        for v in arvores[i]:
            antigosVertices.append(v["vertice"])
            nv += "{},".format(v["vertice"])
        nv = nv[:-1]
        nv += ">"
        novosVertices.append({"vertice":i, "tag":nv, "antigosVertices":tuple(antigosVertices)})

    # Criar as novas arestas
    novasArestas = []
    for i in range(0, len(novosVertices)):
        for j in range(0, len(novosVertices)):
            if(i == j):
                continue

            for a in range(0, len(novosVertices[i]["antigosVertices"])):
                u = novosVertices[i]["antigosVertices"][a]
                for b in range(0, len(novosVertices[j]["antigosVertices"])):
                    v = novosVertices[j]["antigosVertices"][b]
                    t = (u, v, 1) in g.arestas
                    if(t):
                        novasArestas.append((i, j, 1))

    nvG = deepcopy(g)
    nvG.vertices = novosVertices[:]
    nvG.arestas = novasArestas[:]
    nvG.nVertices = len(novosVertices)
    nvG.nArestas = len(novasArestas)

    if(g.tipoEstrutura == 0):
        nvG.gerarMA()
    elif(g.tipoEstrutura == 1):
        nvG.gerarLA()
    elif(g.tipoEstrutura == 2):
        nvG.gerarMI()

    return(nvG)
