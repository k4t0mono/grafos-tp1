# ToDo

## New

- [ ] Classe Grafo
  - [ ] Construtor
    - [ ] Função que gera lista de adjacencia
    - [ ] Função que gera matriz de incidencia
  - [ ] Grafo.vizinhos()
    - [ ] Pegar vizinhos para lista de adjacencia
    - [ ] Pegar vizinhos para matriz de incidencia
  - [ ] Grafo.inserirAresta()
    - [ ] Inserir aresta para lista de adjacencia
    - [ ] Inserir aresta para matriz de incidencia
  - [ ] Grafo.removerAresta()
    - [ ] Remover aresta para lista de adjacencia
    - [ ] Remover aresta para matriz de incidencia
  - [ ] Grafo.__str__
    - [ ] Imprimir grafo para lista de adjacencia
    - [ ] Imprimir grafo para matriz de incidencia

## Legacy

- Estrutura padrão tem ser matriz de adjacencia
- Vertices na `ma` tem ser uma lista `a = [bool, peso, estado]`
- Função para pegar os vizinhos de v
- Função de remover vértice
- Função para resetar estados
- Função de adicionar arestas
- Modificar AcharPontes() para AcharVerticesDeArticulacao()
