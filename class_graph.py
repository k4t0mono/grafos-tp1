#!/usr/bin/env python3
# coding=utf-8

class Grafo:

    def __init__(self, arq:str, estrutura:int = 0):
        """Constroi o grafo aparitir do arquivo

        estrutura = 0 : Matriz de adjacencia
                    1 : Lista de adjacencia
                    2 : Matriz de incidencia
        """

        # Abre o arquivo
        fl = open(arq, "r")
        # Ler o arquivo
        lines = fl.read().splitlines()

        # Armazenar as caracteristicas do grafo
        self.direcionado = True if lines[1] == "DIRECTED" else False
        self.valorado = True if lines[2] == "WEIGHTED" else False
        self.nVertices = int(lines[3])

        # Arestas
        self.arestas = []
        # Ler as linhas com as arestas
        for i in range(4, len(lines)):
            # Ler os vertices
            tmp = lines[i].split()
            tmp[0] = int(tmp[0])
            tmp[1] = int(tmp[1])

            # aresta = (v1, v2, peso)
            a = [tmp[0], tmp[1], 1]
            aI = [tmp[1], tmp[0], 1]

            # ser for valorado, colocar a peso
            if self.valorado:
                a[2] = (tmp[2])
                aI[2] = (tmp[2])

            # transfomar a lista em uma tupla
            a = tuple(a)
            aI = tuple(aI)

            # se não for direcionado, não repetir aresta
            if(not self.direcionado and (aI in self.arestas)):
                continue

            # adicionar aresta na lista de arestas
            self.arestas.append(a)

        self.nArestas = len(self.arestas)

        if(estrutura == 0):
            self.tipoEstrutura = 0
            self.gerarMA()
        elif(estrutura == 1):
            self.gerarLA()
        elif(estrutura == 2):
            self.gerarMI()
        else:
            print("Estrutura não válida")

    def gerarMA(self):
        """Gera a Matriz de adjacencia do grafo"""

        self.tipoEstrutura = 0
        # criar uma lista vazia
        self.estrutura = []
        for i in range(0, self.nVertices):
            self.estrutura.append([])
            for j in range(0, self.nVertices):
                self.estrutura[i].append(None)

        # inserir as arestas
        for i in self.arestas:
            self.estrutura[i[0]][i[1]] = i[2]
            if(not self.direcionado):
                self.estrutura[i[1]][i[0]] = i[2]

    def gerarMI(self):
        """Gerar a matriz de incidencia"""

        self.tipoEstrutura = 2
        # Criar um lista vazia
        self.estrutura = []
        for i in range(0, self.nArestas):
            self.estrutura.append([])
            for j in range(0, self.nVertices):
                self.estrutura[i].append(None)

        _vertices = []
        for i in range(0, self.nVertices):
            _vertices.append([])


        for a in range(0, self.nArestas):
            v1 = self.arestas[a][0]
            v2 = self.arestas[a][1]
            w = self.arestas[a][2]

            _vertices[v1].append((v2, a))

            print("{}: {}".format(a, self.arestas[a]))
            self.estrutura[a][v1] = w
            if(self.direcionado and (v1 != v2)):
                self.estrutura[a][v2] = -w
            else:
                self.estrutura[a][v2] = w

        print("_vertices: {}".format(_vertices))

    def inserirAresta(self, v1, v2, peso=1):
        """Inserir uma aresta nova no grafo"""

        self.arestas.append((v1, v2, peso))

        if(self.tipoEstrutura == 0):
            self.estrutura[v1][v2] = peso
            if(not self.direcionado):
                self.estrutura[v2][v1] = peso

        elif(self.tipoEstrutura == 2):
            self.estrutura.append([])
            for i in range(0, self.nVertices):
                self.estrutura[-1].append(None)
            self.estrutura[-1][v1] = peso
            if(self.direcionado and (v1 != v2)):
                self.estrutura[-1][v2] = -peso
            else:
                self.estrutura[-1][v2] = peso

    def removerAresta(self, v1, v2):
        """Remove uma aresta do grafo"""

        r = [(x,y,z) for x,y,z in self.arestas if((x == v1) and (y == v2) or (x == v2) and (y == v1))]
        if(not len(r)):
            raise "Aresta não existe"
        elif(len(r) > 1):
            raise "erro desconhecido"
        else:
            self.arestas.remove(r[0])

        if(self.tipoEstrutura == 0):
            self.estrutura[v1][v2] = None
            if(not self.direcionado):
                self.estrutura[v2][v1] = None

    def vizinhos(self, v: int):
        """Retorna a lista de vizinhos de v"""

        vizinhos = []

        if(self.tipoEstrutura == 0):
            for i in range(0, self.nVertices):
                if(self.estrutura[v][i]):
                    vizinhos.append(i)

        return vizinhos

    def __str__(self):
        a = ""
        if(self.tipoEstrutura == 0):
            for i in self.estrutura:
                for j in i:
                    if(j is None):
                        a += "0    "
                    elif(type(j) == float):
                        if(j == float('inf')):
                            a += "I    "
                        elif(j == float('-inf')):
                            a += "-I   "
                        else:
                            a += "{:^5} ".format(j)
                    else:
                        a += "{:4} ".format(str(j))

                a += "\n"

        elif(self.tipoEstrutura == 2):
            for i in self.estrutura:
                for j in i:
                    if(j == None):
                        a += "{:5}".format(0)
                    else:
                        a += "{:5}".format(j)
                a += "\n"


        return a.rstrip()
