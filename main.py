#!/usr/bin/env python3

import sys

from class_graph import *
from functions import *

# if(len(sys.argv) < 3):
#     print("Argumentos insuficientes")
#     # print("Argumentos insuficientes", file=sys.stderr)
#     print("./main <graph.txt>")
#     # sys.exit(11)

g = Grafo(sys.argv[1])

if("-p1" in sys.argv):
    if(not g.direcionado):
        print(BuscarPontes(g))

    else:
        print("O grafo é direcionado")
        # print("O grafo é direcionado", file=sys.stderr)
        # sys.exit(12)

if("-print" in sys.argv):
    print(g, end="\n\n")

if("-p2" in sys.argv):
    if(not g.direcionado):
        print(BuscarVerticesArticulacao(g))

    else:
        print("O grafo é direcionado")
        # print("O grafo é direcionado", file=sys.stderr)
        # sys.exit(12)

if("-p3" in sys.argv):
    print(AlcansarNVertices(g, int(sys.argv[sys.argv.index("-p3")+1])))

if("-dfs" in sys.argv):
    print(DFS(g, int(sys.argv[sys.argv.index("-dfs")+1])))

if("-w" in sys.argv):
    w = Warshall(g)

    for i in range(0, g.nVertices):
        for j in range(0, g.nVertices):
            a = w[i][j]
            print("{:5}".format(a if a!= None else 0), end="")

        print("")


if("-p5" in sys.argv):
    w = Warshall(g)

    if(GrafoFConexo(w)):
        print("Grafo F Conexo")
    else:
        if(GrafoSFConexo(w)):
            print("Grafo SF Conexo")
        else:
            if(GrafoSConexo(g)):
                print("Grafo S Conexo")
            else:
                print("Grafo Desconexo")

if("-p6" in sys.argv):
    g2 = ComponenteFConexa(g)
    print(g2)

if("-gmi" in sys.argv):
    print("nVertices: {}".format(g.nVertices))
    print("nArestas: {}".format(g.nArestas))
    g1 = Grafo(sys.argv[1], 2)
    g1.inserirAresta(0, 3)
    print(g1)
